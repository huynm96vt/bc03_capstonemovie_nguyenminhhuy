let USER = "USER";

export const localStorageService = {
  setUserInfo: (user) => {
    let dataJson = JSON.stringify(user);
    localStorage.setItem(USER, dataJson);
  },
  getUserInfo: () => {
    let dataJSON = localStorage.getItem(USER);

    if (dataJSON) {
      return JSON.parse(dataJSON);
    }
    return null;
  },
  removeUserInfo: () => {
    return localStorage.removeItem(USER);
  },
};
