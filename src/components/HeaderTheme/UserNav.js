import React from "react";
import { useSelector } from "react-redux";
import { localStorageService } from "../../services/localStorageService";

export default function UserNav() {
  let userInfo = useSelector((state) => state.userReducers.userInfo);

  let handleLogOut = () => {
    localStorageService.removeUserInfo();
    window.location.href = "/";
  };

  let handleLogin = () => {
    window.location.href = "/login";
  };

  let handleRegister = () => {
    window.location.href = "/register";
  };
  return (
    <div>
      {userInfo ? (
        <div className=" space-x-3">
          <span>{userInfo.hoTen}</span>
          <button
            onClick={handleLogOut}
            className=" border-gray-700 border-2 px-4 py-2 rounded text-gray-700"
          >
            Đăng xuất
          </button>
        </div>
      ) : (
        <div className="flex">
          <button
            onClick={handleLogin}
            className=" flex items-center justify-end px-3 py-1  text-gray-400 border-r-2 border-gray-300 hover:text-red-500 transition-all duration-300"
          >
            <svg
              style={{
                width: "28px",
                height: "28px",
                margin: "0 8px",
              }}
              viewBox="0 0 512 512"
            >
              <path
                style={{ fill: "currentColor" }}
                d="M256 0C114.6 0 0 114.6 0 256s114.6 256 256 256s256-114.6 256-256S397.4 0 256 0zM256 128c39.77 0 72 32.24 72 72S295.8 272 256 272c-39.76 0-72-32.24-72-72S216.2 128 256 128zM256 448c-52.93 0-100.9-21.53-135.7-56.29C136.5 349.9 176.5 320 224 320h64c47.54 0 87.54 29.88 103.7 71.71C356.9 426.5 308.9 448 256 448z"
              />
            </svg>
            Đăng nhập
          </button>

          <button
            onClick={handleRegister}
            className=" flex items-center justify-end px-3 py-1  text-gray-400  hover:text-red-500 transition-all duration-300"
          >
            <svg
              style={{
                width: "28px",
                height: "28px",
                margin: "0 8px",
              }}
              viewBox="0 0 512 512"
            >
              <path
                style={{ fill: "currentColor" }}
                d="M256 0C114.6 0 0 114.6 0 256s114.6 256 256 256s256-114.6 256-256S397.4 0 256 0zM256 128c39.77 0 72 32.24 72 72S295.8 272 256 272c-39.76 0-72-32.24-72-72S216.2 128 256 128zM256 448c-52.93 0-100.9-21.53-135.7-56.29C136.5 349.9 176.5 320 224 320h64c47.54 0 87.54 29.88 103.7 71.71C356.9 426.5 308.9 448 256 448z"
              />
            </svg>
            Đăng ký
          </button>
        </div>
      )}
    </div>
  );
}
