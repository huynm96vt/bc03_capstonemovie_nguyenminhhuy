import React from "react";
import { NavLink } from "react-router-dom";
import UserNav from "./UserNav";

export default function HeaderTheme() {
  return (
    <div
      className="fixed z-50 h-20 w-full flex items-center justify-between shadow-lg px-20"
      style={{ backgroundColor: "rgba(255,255,255,0.95)" }}
    >
      <NavLink to="/">
        <img
          className="h-20 w-48"
          src="https://png.pngtree.com/png-clipart/20190516/original/pngtree-cinema-vector-illustration-png-image_3704537.jpg"
          alt=""
        />
      </NavLink>
      <div className=" space-x-5 text-lg font-medium text-gray-600">
        <a>Tin tức</a>
        <a>Cụm rạp</a>
        <a>Lịch chiếu</a>
      </div>
      <UserNav />
    </div>
  );
}
