import React from "react";
import Lottie from "lottie-react";
import FormLogin from "./FormLogin/FormLogin";
import loginAnimate from "../../assets/login-animate.json";

export default function LoginPage() {
  return (
    <div className="w-screen h-screen bg-blue-300 flex items-center">
      <div className="w-1/2 h-full bg-red-500">
        <Lottie animationData={loginAnimate} loop={false} />
      </div>
      <div className="w-1/2 mx-auto p-5 rounded bg-white">
        <FormLogin />
      </div>
    </div>
  );
}
