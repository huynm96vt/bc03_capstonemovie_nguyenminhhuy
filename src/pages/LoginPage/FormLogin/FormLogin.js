import React from "react";
import { Button, Form, Input, message } from "antd";
import { userService } from "../../../services/userService";
import { localStorageService } from "../../../services/localStorageService";
import { NavLink, useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  setUserInfoAction,
  setUserInfoActionServ,
} from "../../../redux/actions/userActions";

export default function FormLogin() {
  let history = useHistory(); //Can thiệp thanh URL, dùng để điều hướng trang
  let dispatch = useDispatch();
  const onFinish = (values) => {
    let onSuccess = () => {
      message.success("OK");
      localStorageService.setUserInfo(values);
      setTimeout(() => {
        history.push("/");
      }, 2000);
    };

    let onFail = () => {
      message.error("Nhập lại");
    };

    dispatch(setUserInfoActionServ(values, onSuccess, onFail));
    // userService
    //   .postDangNhap(values)
    //   .then((res) => {
    //     console.log(res);
    //     localStorageService.setUserInfo(res.data.content);
    //     dispatch(setUserInfoAction(res.data.content));
    //     message.success("Đăng nhập thành công");
    //     setTimeout(() => {
    //       history.push("/");
    //     }, 2000);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     message.error(err.response.data.content);
    //   });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      name="basic"
      layout="vertical"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Tài khoản"
        name="taiKhoan"
        rules={[
          {
            required: true,
            message: "Please input your username!",
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Mật khẩu"
        name="matKhau"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <div className=" text-right">
        <NavLink to="/register">
          <span className=" text-blue-600">Chưa có tài khoản? Đăng ký</span>
        </NavLink>
      </div>

      <div className="text-center">
        <Button className="bg-blue-500 text-white " htmlType="submit">
          Đăng nhập
        </Button>
      </div>
    </Form>
  );
}
