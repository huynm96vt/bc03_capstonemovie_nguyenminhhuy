import { Tabs } from "antd";
import { useEffect, useState } from "react";
import { movieService } from "../../../services/movieService";
import MovieTabItem from "./MovieTabItem";

const { TabPane } = Tabs;

const onChange = (key) => {
  console.log(key);
};

const MovieTabs = () => {
  let [dataRaw, setDataRaw] = useState([]);
  useEffect(() => {
    movieService
      .getMovieByTheater()
      .then((res) => {
        setDataRaw(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderContent = () => {
    return dataRaw.map((heThongRap, index) => {
      return (
        <TabPane
          tab={<img className="w-10 h-10" src={heThongRap.logo}></img>}
          key={index}
        >
          <Tabs
            style={{ height: 500 }}
            tabPosition="left"
            defaultActiveKey="1"
            onChange={onChange}
          >
            {heThongRap.lstCumRap.map((cumRap, index) => {
              return (
                <TabPane
                  style={{ height: 500, overflowY: "scroll" }}
                  tab={
                    <div className="w-48 whitespace-normal text-left">
                      <p>{cumRap.tenCumRap}</p>
                      <p>{cumRap.diaChi}</p>
                    </div>
                  }
                  key={index}
                >
                  {cumRap.danhSachPhim.map((phim, index) => {
                    return <MovieTabItem key={index} movie={phim} />;
                  })}
                </TabPane>
              );
            })}
          </Tabs>
        </TabPane>
      );
    });
  };
  return (
    <Tabs
      style={{ height: 500 }}
      tabPosition="left"
      defaultActiveKey="1"
      onChange={onChange}
    >
      {renderContent()}
      {/* <TabPane tab="Tab 1" key="1">
        Content of Tab Pane 1
      </TabPane>
      <TabPane tab="Tab 2" key="2">
        Content of Tab Pane 2
      </TabPane>
      <TabPane tab="Tab 3" key="3">
        Content of Tab Pane 3
      </TabPane> */}
    </Tabs>
  );
};

export default MovieTabs;
