import React from "react";

export default function MovieTabItem({ movie }) {
  return (
    <div className="flex p-5 border-b border-gray-500 space-x-10">
      <img src={movie.hinhAnh} alt="" className="w-20" />
      <p>{movie.tenPhim}</p>
    </div>
  );
}
