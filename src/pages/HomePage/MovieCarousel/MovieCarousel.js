import { Carousel } from "antd";
import MovieItem from "../MovieItem/MovieItem";

const MovieCarousel = ({ chunkedList }) => {
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };

  return (
    <Carousel afterChange={onChange}>
      {chunkedList.map((movies, index) => {
        return (
          <div key={index} className=" h-max w-full">
            <div className="grid grid-cols-4 gap-10 py-20 ">
              {movies.map((item) => {
                return <MovieItem movie={item} />;
              })}
            </div>
          </div>
        );
      })}
    </Carousel>
  );
};

export default MovieCarousel;
