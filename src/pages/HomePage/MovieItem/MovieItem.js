import { Card } from "antd";
import moment from "moment";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

const MovieItem = ({ movie }) => {
  console.log(movie);
  return (
    <Card
      hoverable
      style={{
        width: "100%",
        maxHeight: "600px",
        overflow: "hidden",
        borderRadius: "10px",
      }}
      cover={
        <img style={{ height: "435px" }} alt="example" src={movie.hinhAnh} />
      }
      className="h-max"
    >
      <Meta
        title={movie.tenPhim}
        description={
          <span className="text-lg font-medium text-red-500">
            {moment(movie.ngayKhoiChieu).format("DD/MM/YYYY")}
          </span>
        }
      />
      <NavLink
        to={`detail/${movie.maPhim}`}
        className=" bg-red-500 text-lg text-white px-5 py-2 rounded mt-5 inline-block hover:text-black"
      >
        Xem chi tiết
      </NavLink>
    </Card>
  );
};

export default MovieItem;
