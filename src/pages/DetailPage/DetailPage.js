import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { movieService } from "../../services/movieService";
import { Progress } from "antd";

export default function DetailPage() {
  let { id } = useParams();
  const [movie, setMovie] = useState({});

  useEffect(() => {
    movieService
      .getDetailMovie(id)
      .then((res) => {
        console.log(res);
        setMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className=" container mx-auto py-10 space-y-10">
      <div className="flex items-center space-x-20">
        <img src={movie.hinhAnh} alt="" className="w-60 rounded" />
        <Progress
          strokeColor={{
            "0%": "#108ee9",
            "100%": "#87d068",
          }}
          type="circle"
          width={200}
          percent={movie.danhGia * 10}
          strokeWidth={10}
          format={(percent) => {
            return (
              <span className=" text-blue-600" style={{ fontSize: 60 }}>
                {" "}
                {percent / 10}
              </span>
            );
          }}
        />
      </div>
      <p>{movie.tenPhim}</p>
      <p>{movie.moTa}</p>
    </div>
  );
}

// {
//   "maPhim": 9165,
//   "tenPhim": "Godzilla123",
//   "biDanh": "godzilla123",
//   "trailer": "https://www.youtube.com/watch?v=SHvrEdqYXHg",
//   "hinhAnh": "https://movienew.cybersoft.edu.vn/hinhanh/godzilla_gp01.jpg",
//   "moTa": "The Earth is headed for disaster and when an archeological research team visits Infant Island to find out why, they discover two tiny women who reveal that the Earth is fighting back for all the harm humans have done here and sends out the evil Battra to destroy us. The Cosmos, as the girls are called, offer their help by calling Mothra to battle the creature. Unfortunately, Godzilla also appears and a three way battle begins that threatens to destroy Japan.",
//   "maNhom": "GP01",
//   "ngayKhoiChieu": "2022-06-25T22:12:21.92",
//   "danhGia": 0,
//   "hot": true,
//   "dangChieu": false,
//   "sapChieu": false
// }
