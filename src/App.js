import "./App.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { userRoutes } from "./routes/userRoutes";

function App() {
  return (
    <div className="">
      <BrowserRouter>
        <Switch>
          {userRoutes.map((route, index) => {
            if (route.isUseLayout) {
              return (
                <Route
                  key={index}
                  exact={route.exact}
                  path={route.path}
                  render={() => {
                    return route.component;
                  }}
                />
              );
            }
            return (
              <Route
                key={index}
                exact={route.exact}
                path={route.path}
                component={route.component}
              />
            );
          })}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
