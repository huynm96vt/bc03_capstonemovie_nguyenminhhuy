import { userService } from "../../services/userService";
import { SET_USER_INFO } from "../constants/userConstants";

export const setUserInfoAction = (user) => {
  return {
    type: SET_USER_INFO,
    payload: user,
  };
};

export const setUserInfoActionServ = (
  dataLogin,
  handleSuccess = () => {},
  handleFail = () => {}
) => {
  return (dispatch) => {
    userService
      .postDangNhap(dataLogin)
      .then((res) => {
        // console.log(res);
        handleSuccess();
        dispatch({
          type: SET_USER_INFO,
          payload: res.data.content,
        });
      })
      .catch((err) => {
        handleFail();
        console.log(err);
      });
  };
};
